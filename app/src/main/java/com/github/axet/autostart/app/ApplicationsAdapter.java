package com.github.axet.autostart.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.axet.autostart.R;

import java.util.ArrayList;

public class ApplicationsAdapter extends BaseAdapter {
    public Context context;
    public ArrayList<ApplicationInfo> packages = new ArrayList<>();

    public ApplicationsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return packages.size();
    }

    @Override
    public Object getItem(int position) {
        return packages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.application_item, parent, false);
        }
        final ApplicationInfo info = packages.get(position);
        PackageManager pm = context.getPackageManager();
        ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
        TextView text = (TextView) convertView.findViewById(R.id.item_text);
        TextView sum = (TextView) convertView.findViewById(R.id.item_summary);

        String n = BootApplication.getApplicationName(pm, info);
        Drawable d = info.loadIcon(pm);

        icon.setImageDrawable(d);
        text.setText(n);
        sum.setText(info.packageName);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        return convertView;
    }
}
