package com.github.axet.autostart.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.content.SharedPreferencesCompat;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.autostart.R;
import com.github.axet.autostart.services.OnBootReceiver;

import java.util.ArrayList;
import java.util.List;

public class BootApplication extends MainApplication {

    public static final String PREF_BOOT = "boot";
    public static final String PREF_REBOOT = "reboot";

    public AlarmManager am;
    public ArrayList<ApplicationInfo> packages = new ArrayList<>();

    public static com.github.axet.autostart.app.BootApplication from(Context context) {
        return (com.github.axet.autostart.app.BootApplication) MainApplication.from(context);
    }

    public static boolean isQuery(PackageManager pm, Intent intent) {
        List<ResolveInfo> ll = pm.queryBroadcastReceivers(intent, PackageManager.GET_RESOLVED_FILTER);
        if (ll != null && ll.size() > 0)
            return true;
        ll = pm.queryIntentServices(intent, PackageManager.GET_RESOLVED_FILTER);
        if (ll != null && ll.size() > 0)
            return true;
        return false;
    }

    public static boolean isExternal(ApplicationInfo info) {
        return (info.flags & ApplicationInfo.FLAG_EXTERNAL_STORAGE) == ApplicationInfo.FLAG_EXTERNAL_STORAGE;
    }

    public static boolean isPackageUnavailable(PackageManager pm, String name) { // ApplicationInfo.isPackageUnavailable
        try {
            return pm.getPackageInfo(name, 0) == null;
        } catch (PackageManager.NameNotFoundException ignore) {
            return true;
        }
    }

    public static String getApplicationName(PackageManager pm, String name) {
        try {
            return getApplicationName(pm, pm.getApplicationInfo(name, PackageManager.GET_META_DATA));
        } catch (PackageManager.NameNotFoundException e) {
            return name;
        }
    }

    public static String getApplicationName(PackageManager pm, ApplicationInfo info) {
        return pm.getApplicationLabel(info).toString();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        am = new AlarmManager(this);
        load();
    }

    public void load() {
        packages = new ArrayList<>();
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> pp = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES); // sometimes sdcard apps not mounted, listed as uninstalled
        for (ApplicationInfo info : pp) {
            if (info.packageName.equals(getPackageName()))
                continue;
            if (isExternal(info)) {
                Intent boot = new Intent(Intent.ACTION_BOOT_COMPLETED);
                boot.setPackage(info.packageName);
                if (isPackageUnavailable(pm, info.packageName) || isQuery(pm, boot)) // uninstalled apps do not query
                    packages.add(info);
            }
        }
    }
}
