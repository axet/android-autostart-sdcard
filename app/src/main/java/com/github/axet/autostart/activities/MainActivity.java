package com.github.axet.autostart.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.autostart.R;
import com.github.axet.autostart.app.ApplicationsAdapter;
import com.github.axet.autostart.app.BootApplication;
import com.github.axet.autostart.services.OnBootReceiver;

public class MainActivity extends AppCompatThemeActivity {
    public static final String ACTION_USB_STATE = "android.hardware.usb.action.USB_STATE"; // UsbManager.ACTION_USB_STATE@hide

    ApplicationsAdapter adapter;
    Handler handler = new Handler();
    BroadcastReceiver mounted = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handler.removeCallbacks(reload);
            for (int i = 0; i < 3; i++)
                handler.postDelayed(reload, i * 2000);
        }
    };
    Runnable reload = new Runnable() {
        @Override
        public void run() {
            reload();
        }
    };

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    public int getAppTheme() {
        return R.style.AppThemeLight_NoActionBar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adapter = new ApplicationsAdapter(this);
        ListView list = (ListView) findViewById(R.id.content);
        list.setAdapter(adapter);
        list.setEmptyView(findViewById(R.id.title));

        SuperUser.rootTest(); // ask for SuperUser perms
        OnBootReceiver.boot(this);

        IntentFilter ff = new IntentFilter();
        ff.addAction(Intent.ACTION_MEDIA_MOUNTED);
        ff.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        ff.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
        ff.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        ff.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        ff.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        ff.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
        ff.addAction(ACTION_USB_STATE);
        registerReceiver(mounted, ff);

        reload();

        if (OptimizationPreferenceCompat.needBootWarning(this, BootApplication.PREF_BOOT))
            OptimizationPreferenceCompat.buildBootWarning(this, BootApplication.PREF_BOOT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.settings:
                SettingsActivity.startActivity(this);
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mounted);
        handler.removeCallbacksAndMessages(null);
    }

    public void reload() {
        BootApplication app = BootApplication.from(this);
        app.load();
        adapter.packages = app.packages;
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reload();
    }
}
