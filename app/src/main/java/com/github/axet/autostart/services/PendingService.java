package com.github.axet.autostart.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.SuperUser;
import com.github.axet.autostart.BuildConfig;
import com.github.axet.autostart.app.BootApplication;

import java.util.ArrayList;

public class PendingService extends IntentService {
    public static final String TAG = PendingService.class.getSimpleName();

    public static final int DELAY = 5000;
    public static final String ACTION_START = BuildConfig.APPLICATION_ID + ".services.action.START";

    public static void pending(Context context, ArrayList<String> name, int count) {
        Log.d(TAG, "pending " + count);
        BootApplication app = BootApplication.from(context);
        AlarmManager am = app.am;
        am.set(System.currentTimeMillis() + DELAY, new Intent(context, PendingService.class)
                .setAction(ACTION_START)
                .putExtra("name", name)
                .putExtra("count", count));
    }

    public PendingService() {
        super("PendingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (action.equals(ACTION_START)) {
                ArrayList<String> names = intent.getStringArrayListExtra("name");
                ArrayList<String> pending = new ArrayList<>();
                for (String s : names) {
                    if (!OnBootReceiver.start(this, s))
                        pending.add(s);
                }
                if (!pending.isEmpty()) {
                    int count = intent.getIntExtra("count", 0) + 1;
                    if (count >= 10) { // unable to pending app for 10 tries, reboot device
                        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
                        if (shared.getBoolean(BootApplication.PREF_REBOOT, true))
                            SuperUser.reboot(); // do not exit, keep retyring, reboot may fails on first try
                        else
                            return; // stop retrying, exit
                    }
                    pending(this, pending, count);
                }
            }
        }
    }
}
