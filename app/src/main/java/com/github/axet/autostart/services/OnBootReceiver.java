package com.github.axet.autostart.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.autostart.app.BootApplication;

import java.util.ArrayList;

public class OnBootReceiver extends BroadcastReceiver {
    public static final String TAG = OnBootReceiver.class.getSimpleName();

    public static boolean start(Context context, String name) {
        PackageManager pm = context.getPackageManager();
        if (BootApplication.isPackageUnavailable(pm, name)) {
            Log.d(TAG, "pending " + name);
            return false;
        }
        Intent intent = new Intent(Intent.ACTION_BOOT_COMPLETED); // Manifest.permission.RECEIVE_BOOT_COMPLETED?
        intent.setPackage(name);
        if (!BootApplication.isQuery(pm, intent)) {
            Log.d(TAG, "cancel " + name);
            return true;
        }

        Log.d(TAG, "starting " + name);
        intent = new Intent(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
        intent.setPackage(name);
        if (BootApplication.isQuery(pm, intent)) {
            context.sendBroadcast(intent);
        } else {
            intent = pm.getLaunchIntentForPackage(name);
            context.startActivity(intent);
        }
        Toast.Error(context, "Starting " + BootApplication.getApplicationName(pm, name));
        return true;
    }

    public static void boot(Context context) {
        BootApplication app = BootApplication.from(context);
        app.load();
        ArrayList<String> pending = new ArrayList<>();
        for (ApplicationInfo info : app.packages) {
            if (!start(context, info.packageName))
                pending.add(info.packageName);
        }
        if (!pending.isEmpty())
            PendingService.pending(context, pending, 0);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        OptimizationPreferenceCompat.setPrefTime(context, BootApplication.PREF_BOOT, System.currentTimeMillis());
        boot(context);
    }

}
