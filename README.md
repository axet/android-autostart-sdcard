# Autostart sdcard

Old android devices has low phone memory sometimes not even enougth to install one additional application in phone memory. Installing apps on external sdcard is the only option. But applications on sdcard has it's own limitation:
applications can not receive boot event.

Autostart translate this event to apps installed on sdcard when it attached by OS.

When android finish boot, it sends android.intent.action.BOOT_COMPLETED to all apps installed on phone memory.
This is system event, it can not be sent or translated by normal application causing SecurityException.
But next event when sdcard mounted android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE fired to the phone apps also,
but this event can be resend/translated using Context.sendBroadcast.

To make your sdcard app works and receive boot event you need to implement booth BOOT_COMPLETED and EXTERNAL_APPLICATIONS_AVAILABLE to work propertly with this app.

# Manual install

    gradle installDebug

# Screenshots

  ![screen1](/docs/screen1.png) ![screen2](/docs/screen2.png)
